/**
 * @param {string} str
 * @param {Object?} data
 * @param {string?} defaultValue
 * @return {string} injectedString
 */
function parser(str, data, defaultValue) {
  defaultValue = defaultValue || '<nothing>';
  data = data || {};

  return str.replace(/{{\s*(.+?)\s*}}/g, function() {
    var replacement = defaultValue
      , segments = arguments[1].split('.');

    if(segments && segments.length) {
        // " You can assume that the example object’s
        //   top-level keys will always be a hexadecimal
        //   key of length 8 characters "
        //                               - @johnmjackson
        //
        // Not sure if this is a requirement or not,
        //   but let's just validate it for fun
        if(!segments[0].match(/^[0-9a-f]{8}$/i)) {
            throw new Error('nonuniform top-level key');
        }

        // Step by step finding the element
        replacement = data;
        for(var i=0, n=segments.length; i<n; ++i) {
            if('undefined' == typeof replacement[segments[i]]) {
                replacement = defaultValue;
                break;
            } else {
                replacement = replacement[segments[i]];
            }
        }

        // Final tuning, if the replacement
        //   is not a beautiful string yet
        if(!replacement) {
            replacement = '';
        } else if(Array == replacement.constructor) {
            replacement = replacement.join(', ');
        } else if('object' == typeof replacement) {
            replacement = JSON.stringify(replacement);
        }
    }

    return replacement;
  })
}
