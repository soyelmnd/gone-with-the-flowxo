describe('A template parser', function() {
  it('should exists, of course', function() {
    expect(parser).toBeDefined();
    expect(typeof parser('', {})).toBe('string');
  });

  it('should pass the first challenge', function() {
    expect(
      parser(
        'This is a string with {{ ab49fd20.key_1 }}, including {{ 9822df87.another_key }} and also {{ ab49fd20.key_2 }}',
        { 'ab49fd20': { key_1: 'some data' }, '9822df87': { another_key: 'big data', yet_another_key: 'small data' } }
      )
    )
    .toBe('This is a string with some data, including big data and also <nothing>');
  });

  it('should block nonuniform top-level key', function() {
    expect(function() { parser('key {{ invalid }}') }).toThrowError(/nonuniform/);
    expect(function() { parser('key {{ ab49fd20a }}') }).toThrowError(/nonuniform/);
    expect(function() { parser('key {{ ab49fd2 }}') }).toThrowError(/nonuniform/);
    expect(function() { parser('key {{ ab49xd20 }}') }).toThrowError(/nonuniform/);

    expect(function() { parser('key {{ ab49fd20 }}') }).not.toThrow();
    expect(function() { parser('key {{ AB49FD20 }}') }).not.toThrow();
  });

  it('should handle object with any depth', function() {
    expect(parser('level {{ ab49fd20 }}', { ab49fd20: 'zero' })).toBe('level zero')
    expect(parser('level {{ ab49fd20.o }}', { ab49fd20: { o: 'one' } })).toBe('level one')
    expect(parser('level {{ ab49fd20.o.t }}', { ab49fd20: { o: { t: 'two' } } })).toBe('level two')
    expect(parser('level {{ ab49fd20.o.t.t }}', { ab49fd20: { o: { t: { t: 'three'  } } } })).toBe('level three')
  });

  it('should use a default value if undefined', function() {
    expect(parser('{{ ab49fd20 }} as always')).toBe('<nothing> as always');
    expect(parser('{{ ab49fd20.key }} as always', { ab49fd20: {} })).toBe('<nothing> as always');
    expect(parser('{{ ab49fd20 }} as always', {}, 'amazing')).toBe('amazing as always');
  });

  it('should post-process ugly value', function() {
    expect(parser('{{ ab49fd20 }} is an array', { ab49fd20: [1, 2, 3] })).toBe('1, 2, 3 is an array');
    expect(parser('{{ ab49fd20 }} is an object', { ab49fd20: { key: 'is value' } })).toBe('{"key":"is value"} is an object');
    expect(parser('"{{ ab49fd20 }}" is empty', { ab49fd20: null })).toBe('"" is empty');
    expect(parser('"{{ ab49fd20 }}" is default', {})).toBe('"<nothing>" is default');
  });
});
