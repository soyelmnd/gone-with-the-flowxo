// First, we have `posts` collection
db.posts.save({
  title: 'All about a blog',
  slug: 'all-about-a-blog'
  body: 'This is not another dummy lorem-ipsum text.',
  tags: [
    'javascript',
    'and',
    'passion'
  ],
  createdAt: new Date(),
  updatedAt: null
});

// Then, to find all posts has `javascript` tag
//   (or replace `find` by `count` to grab the number)
db.posts.find({
  tags: {
    $in: ['javascript']
  }
});

// However, as document-count for tags maybe handy
//   especially when we need a dynamic sizing
//   tag cloud (more popular bigger)
// We may have a `tags` collection to keep track
//   of all available tags, and the quantity
//   (ie. the number of posts using this tag),
// This will make your life difficult at first,
//   especially when create/update/delete a post,
//   but will become handy later, and also better
//   for performance
['javascript', 'and', 'passion'].each(function(tag) {
  db.tags.findAndModify({
    query: {
      name: tag
    },
    update: {
      $inc: {
        quantity: 1
      },
      $setOnInsert: {
        name: tag
      }
    },
    new: true,
    upsert: true
  });
});

// With that approach, the tag cloud are just
//   a list of tags with their (processed) quantities
db.tags.find();
